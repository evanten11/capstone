const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product Name is required!"]
	},
	description: {
		type: String,
		required: [true, "Product Description is required!"]
	},
	price: {
		type: Number,
		required: [true, "Price is required!"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	userOrders: [
		{
			userId: {
				type: String,
				required: [true, "User ID of the Customer is required!"]
			}
		}
	]
})

const Products = mongoose.model("Product", productSchema);

module.exports = Products;
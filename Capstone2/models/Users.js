const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required!"]
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	},
	password2: {
		type: String,
		required: [true, "Password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orderedProduct: [
		{
			productId: {
				type: String,
				required: [true, "productId is required!"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
			
	]
	
})

const Users = mongoose.model("User", userSchema);

module.exports = Users;


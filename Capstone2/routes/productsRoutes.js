const express = require('express');
const productsControllers = require('../controllers/productsControllers.js');
const auth = require('../auth.js');
const router = express.Router();

// Without Parameters
// Route for adding products
router.post('/addProducts', auth.verify, productsControllers.addProduct);


// Route for retrieving all products(admin only)
router.get('/', productsControllers.getAllProducts);

// Route for retrieving active Products
router.get('/activeProducts', productsControllers.getActiveProducts);

// route for retrieving archived products
router.get('/archivedProducts', auth.verify, productsControllers.getArchivedProducts);


// With Parameters
// route for getting specific product
router.get('/:productId', productsControllers.getProduct);

// route for updating a product
router.patch('/update/:productId', auth.verify, productsControllers.updateProduct);

// route for archive a product
router.patch('/:productId/archive', auth.verify, productsControllers.archiveProduct);

router.patch('/:productId/activate', auth.verify, productsControllers.activateProduct);

module.exports = router;
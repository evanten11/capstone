const Users = require("../models/Users.js");
const bcrypt = require('bcrypt');
const auth = require('../auth.js')
const Products = require('../models/Products.js')


// Register User in NHJ Hardware
module.exports.registerUser = (request, response) => {

	let userPassword = request.body.password;

	Users.findOne({email: request.body.email})
	.then(result => {
		if(result){
			return response.send(false)
		}
		else{
			let newUser = new Users({
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password, 10),
				password2: bcrypt.hashSync(request.body.password, 10),
				isAdmin: request.body.isAdmin
			})

			newUser.save()
			.then(saved => response.send(true))
			.catch(error => response.send(false));
		}
	})
	.catch(error => response.send(false));
}


module.exports.loginUser = (request, response) => {
	Users.findOne({email: request.body.email})
	.then(result => {
		if(!result){
			return response.send(false)
		}
		else{
			
			const isPasswordCorrect = bcrypt.
				compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				return response.send(
					{ 
						auth : auth.createAccessToken(result)
					}
				)
			}
			else{
				return response.send(false)
			}
		}
	})
	.catch(error => response.send(false));
}

// Retrive user details
module.exports.getUserDetails = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		let profileId = request.body.id;

		Users.findById(profileId)
		.then(result => {
			result.password = "*****";
			return response.send(result);
		})
		.catch(error => response.send(false));
	}
	else{
		return response.send(false);
	}
}

// Non-admin User checkout (Create Order)
module.exports.createOrder = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const productId = request.body.products.productId;
	const productName = request.body.products.productName;
	const quantity = request.body.products.quantity;

	const totalAmount = request.body.totalAmount;

	if(userData.isAdmin){
		return response.send(false);
	}
	else{
		let isUserUpdated = Users.findOne({_id: userData.id})
		.then(result => {

			result.orderedProduct.push({
				products: {
					productId: productId,
					productName: productName,
					quantity: quantity
				},
				totalAmount: totalAmount
				
			})
			result.save()
			.then(save => true)
			.catch(error => false)

	
		})
		.catch(error => false)

		let isProductUpdated = Products.findOne({_id: request.body.products.productId})
		.then(result => {
			result.userOrders.push({
				userId: userData.id
			})
			result.save()
			.then(save => true)
			.catch(error => false)
		})
		.catch(error => false)
		
		if(productId == "" || productName == "" || quantity == "" || totalAmount == ""){		
			return response.send(false)
		}
		else if(isUserUpdated && isProductUpdated){		
			return response.send(true)
		}
		else{
			return response.send(false)
		}
	
	}
}

// Set user as admin (admin only)
module.exports.userAsAdmin = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const userId = request.params.userId;

	let admin = {
		isAdmin: request.body.isAdmin
	}

	Users.findByIdAndUpdate(userId, admin)
	.then(result => {
		if(userData.isAdmin){
			if(request.body.isAdmin === true){
				return response.send(true)
			}
			else{
				return response.send(true)
			}
		}
		else{
			return response.send (false)
		}
	})
	.catch(error => response.send(false))
}
	

module.exports.retrieveUserDetails = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	Users.findOne({_id : userData.id})
	.then(data => response.send(data));
}

module.exports.checkout = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const productId = request.body.id;

	if(userData.isAdmin){
		return response.send(false);
	}
	else{
		let isUserUpdated = Users.findOne({_id: userData.id})
		.then(result => {

			result.orderedProduct.push({
				productId: productId
			})
			result.save()
			.then(save => true)
			.catch(error => false)

	
		})
		.catch(error => false)

		let isProductUpdated = Products.findOne({_id: productId})
		.then(result => {
			result.userOrders.push({
				userId: userData.id
			})
			result.save()
			.then(save => true)
			.catch(error => false)
		})
		.catch(error => false)
		
		
		if(isUserUpdated && isProductUpdated){		
			return response.send(true)
		}
		else{
			return response.send(false)
		}
	
	}
}
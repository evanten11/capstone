import {Link} from 'react-router-dom';

export default function PageNotFound(){

	return(
		<>
		<h1 className = 'ms-5 mt-3'>Page Not Found</h1>
		<p className = 'ms-5'> Go back to the
		<Link to = '/' style={{ textDecoration: 'none' }}> homepage</Link>.
		</p>
		</>
	)
}